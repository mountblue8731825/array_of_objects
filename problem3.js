const data = require("./info");

const problem3 = () => {
  data.forEach((info) => {
    if (info.isStudent && info.country.toLowerCase() === "australia") {
      console.log(info.name);
    }
  });
};

problem3();

module.exports = problem3;
