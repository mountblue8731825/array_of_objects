const data = require("./info");

const problem1 = () => {
  const emails = data.map((info) => {
    return info.email;
  });

  return emails;
};

console.log(problem1());
